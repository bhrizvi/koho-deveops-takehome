terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

module "aws" {
  source    = "./aws"
}

terraform {
   required_version = ">= 0.12"
   backend "s3" {
   bucket = "koho-projects-terraform-iac-states"
   key    = "gitlab-template-terraform-devops-takehome-bilalhasan.tfstate"
	 region = "us-east-1"
  }
}