The Project is running on AWS and Deployed in Gitlab in us-east-1 (N. Viginia) region.

Git Repostiory: https://gitlab.com/bhrizvi/koho-deveops-takehome/
Git Pipelines: https://gitlab.com/bhrizvi/koho-deveops-takehome/-/pipelines

To deploy to an AWS account folowing secrets are required to be added in Git Settings
AWS_ACCESS_KEY_ID
AWS_DEFAULT_REGION
AWS_SECRET_ACCESS_KEY

There are 4 stages of Pipeline for this project
stages:
  - validate (Automatic)
  - build (Automatic)
  - deploy (Manual)
  - destroy (Manual)

As soon as the project is pushed to  Gitlab repository the pipeline starts to run automatically. To deploy the resources in AWS, deploy stage has to be triggered manually. Same for destroy.