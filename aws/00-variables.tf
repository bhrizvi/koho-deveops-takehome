# Project name prefix for resources
variable "resource-prefix" {
  default = "koho-devops"
  type = string
  description = "Default resource name prefix in oder not to create resources with the same name for multiple environments"
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 8080
}

variable "app_protocol" {
  default     = "HTTPS"
  type = string
  description = "Protocol exposed by the docker image to redirect traffic to"
}

variable "db_allocated_storage" {
  default     = 100
  description = "RDS DB Allocated Storage"
}

variable "db_storage_type" {
  default     = "gp2"
  type = string
  description = "RDS Storage Type"
}

variable "db_engine" {
  default     = "postgres"
  type = string
  description = "RDS DB Engine"
}

variable "db_engine_version" {
  default     = "12.3"
  type = string
  description = "RDS DB Engine Version"
}

variable "db_identifier" {
  default     = "koho-devops-postgres"
  type = string
  description = "RDS DB Indentifier"
}

variable "db_instance_class" {
  default     = "db.m5.large"
  type = string
  description = "RDS DB Instance Class"
}

variable "db_name" {
  default     = "postgres"
  type = string
  description = "RDS DB Name"
}

variable "db_username" {
  default     = "koho"
  type = string
  description = "RDS DB Username"
}

variable "db_password" {
  default     = "pass12345"
  type = string
  description = "RDS DB Password"
}

# Fargate cpu
variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "256"
}

# Fargate memory
variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "512"
}

# performance tuning for User service:

# Fargate cpu
variable "fargate_cpu_user" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "1024"
}

# Fargate memory
variable "fargate_memory_user" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "1024"
}

# Application Count [Optional] - by default 1
variable "app_count" {
  description = "Number of docker containers to run"
  default     = 2
}

variable "qledger_service_image" {
  default     = "892050973660.dkr.ecr.us-east-1.amazonaws.com/koho-devops/qledger:1.0.0-dev"
  type = string
  description = "Application ECR Service Image"
}



locals {
  common_tags = {
	CreatedBy = "bilal.hasan",
  Project   = "KOHO DevOps TakeHome",
	Comments  = "Provisioned by Terraform"
    }
}