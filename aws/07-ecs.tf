resource "aws_ecs_cluster" "main" {
  name = "${var.resource-prefix}-ecs-cluster"
}


resource "aws_ecs_task_definition" "qledger_service" {
  family                   = "qledger-service-dev"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  execution_role_arn       = aws_iam_role.iam_role_for_ecs_task_execution.arn
  #task_role_arn            = aws_iam_role.iam_role_for_ecs_task_execution.arn

  container_definitions = <<DEFINITION
[
  {
    "cpu": ${var.fargate_cpu},
    "image": "${var.qledger_service_image}",
    "memory": ${var.fargate_memory},
    "name": "qledger-service",
    "networkMode": "awsvpc",
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${var.resource-prefix}-ecs-task-logs",
        "awslogs-region": "us-east-1",
        "awslogs-stream-prefix": "fargate-ecs"
      }
    },"secrets": [
        {
          "valueFrom": "arn:aws:ssm:us-east-1:892050973660:parameter/${var.resource-prefix}-postgres-database",
          "name": "PGDATABASE"
        },
        {
          "valueFrom": "arn:aws:ssm:us-east-1:892050973660:parameter/${var.resource-prefix}-postgres-host",
          "name": "PGHOST"
        },
        {
          "valueFrom": "arn:aws:ssm:us-east-1:892050973660:parameter/${var.resource-prefix}-postgres-password",
          "name": "PGPASSWORD"
        },
        {
          "valueFrom": "arn:aws:ssm:us-east-1:892050973660:parameter/${var.resource-prefix}-postgres-port",
          "name": "PGPORT"
        },
        {
          "valueFrom": "arn:aws:ssm:us-east-1:892050973660:parameter/${var.resource-prefix}-postgres-user",
          "name": "PGUSER"
        }
    ], "portMappings": [
      {
        "containerPort": ${var.app_port},
        "protocol": "tcp",
        "hostPort": ${var.app_port}
      }
    ]
  }
]
DEFINITION
}

resource "aws_ecs_service" "qledger_service" {
  name            = "qledger-service"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.qledger_service.arn
  desired_count   = var.app_count
  platform_version = "1.4.0"
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = [aws_security_group.ecs_tasks.id]
    subnets         = flatten([aws_subnet.private.*.id])
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.qledger_service_tg.id
    container_name   = "qledger-service"
    container_port   = var.app_port
  }

    depends_on = [
    aws_lb_listener.front_end,
  ]

}