resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.main.id
  port              = "8080"
  protocol          = "HTTP"
  #ssl_policy        = "ELBSecurityPolicy-2016-08"
  #certificate_arn   = "arn:aws:acm:eu-central-1:280811547308:certificate/8fd4f290-c060-4ac6-8a82-bd02a9667d84"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "the path you are trying to reach, does not exist"
      status_code  = "503"
    }
  }
}

resource "aws_lb_listener_rule" "qledger_forward" {
  listener_arn = aws_lb_listener.front_end.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.qledger_service_tg.id
  }
  condition {
    path_pattern {
      values = ["/qledger"]
    }
  }
}