resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.devops_vpc.id
}

# Create a NAT gateway with an Elastic IP for each private subnet to get internet connectivity
resource "aws_eip" "gw" {
  count      = 3 #length(data.aws_availability_zones.available.names)
  vpc        = true
  depends_on = [aws_internet_gateway.internet_gateway]

  tags = local.common_tags
}

resource "aws_nat_gateway" "gw" {
  count         = 3 #length(data.aws_availability_zones.available.names)
  subnet_id     = element(flatten(aws_subnet.public.*.id), count.index)
  allocation_id = element(flatten(aws_eip.gw.*.id), count.index)

  tags = local.common_tags
}