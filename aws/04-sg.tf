resource "aws_security_group" "lb" {
  name        = "${var.resource-prefix}-alb"
  description = "controls access to the ALB"
  vpc_id      = aws_vpc.devops_vpc.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Traffic to the EKS Cluster should only come from the ALB
resource "aws_security_group" "eks_tasks" {
  name        = "${var.resource-prefix}-eks-tasks"
  description = "allow inbound access from the ALB only"
  vpc_id      = aws_vpc.devops_vpc.id

  ingress {
    protocol    = "tcp"
    from_port   = 5432
    to_port     = 5432
    security_groups = [aws_security_group.rds.id]
    description = "ALB SG"
  }

  ingress {
    protocol    = "tcp"
    from_port   = 8080
    to_port     = 8080
    security_groups = [aws_security_group.lb.id]
    description = "ALB SG"
  }


  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "rds" {
  name        = "koho-devops-rds"
  description = "Inbound only allowed from EKS-Tasks"
  vpc_id      = aws_vpc.devops_vpc.id

  ingress{
    protocol    = "tcp"
    from_port   = 5432
    to_port     = 5432
    cidr_blocks = ["10.0.0.0/16"]
    description = "EKS-Tasks SG"
  }


  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["10.0.0.0/16"]
    description = "EKS-Tasks SG"
  }
}

resource "aws_security_group" "ecs_tasks" {
  name        = "${var.resource-prefix}-ecs-tasks"
  description = "allow inbound access from the ALB only"
  vpc_id      = aws_vpc.devops_vpc.id

  ingress {
    protocol    = "tcp"
    from_port   = 5432
    to_port     = 5432
    security_groups = [aws_security_group.rds.id]
    description = "ALB SG"
  }

  ingress {
    protocol    = "tcp"
    from_port   = 8080
    to_port     = 8080
    security_groups = [aws_security_group.lb.id]
    description = "ALB SG"
  }


  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}