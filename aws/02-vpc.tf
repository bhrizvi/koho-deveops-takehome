# Create a VPC
resource "aws_vpc" "devops_vpc" {
  cidr_block = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "koho-devops-vpc"
    CreatedBy = "bilal.hasan",
    Project   = "KOHO DevOps TakeHome",
    Comments  = "Provisioned by Terraform"
  }
}