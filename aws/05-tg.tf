resource "aws_lb_target_group" "app" {
  name        = "${var.resource-prefix}-tg"
  port        = var.app_port
  protocol    = var.app_protocol
  target_type = "ip"
  vpc_id      = aws_vpc.devops_vpc.id

   health_check {
    path = "/health"
    protocol = var.app_protocol
    port = var.app_port
    matcher = "200"
  }

  stickiness {
    enabled = false
    type    = "lb_cookie"
  }
}

resource "aws_lb_target_group" "qledger_service_tg" {
  name        = "${var.resource-prefix}-qledger-service-tg"
  port        = var.app_port
  protocol    = var.app_protocol
  target_type = "ip"
  vpc_id      = aws_vpc.devops_vpc.id

   health_check {
    path = "/health"
    protocol = var.app_protocol
    port = var.app_port
    matcher = "200"
  }

  stickiness {
    enabled = false
    type    = "lb_cookie"
  }
}