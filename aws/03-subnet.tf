# Fetch AZs in the current region
data "aws_availability_zones" "available" {}

# Create var.az_count private subnets, each in a different AZ
resource "aws_subnet" "private" {
  count             = 3 #length(data.aws_availability_zones.available.names) selecting only 1a,1b,1c
  cidr_block        = cidrsubnet("10.0.0.0/16", 8, count.index)
  availability_zone = data.aws_availability_zones.available.names[count.index]
  vpc_id            = aws_vpc.devops_vpc.id
  
  tags = local.common_tags
}

# Create var.az_count public subnets, each in a different AZ
resource "aws_subnet" "public" {
  count                   = 3 #length(data.aws_availability_zones.available.names) selecting only 1a,1b,1c
  cidr_block              = cidrsubnet("10.0.0.0/16", 8, count.index + length(data.aws_availability_zones.available.names)) // for not conflicting with private zone
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  vpc_id                  = aws_vpc.devops_vpc.id
  map_public_ip_on_launch = true

  tags = local.common_tags
}