resource "aws_db_instance" "db_instance" {
  allocated_storage    = var.db_allocated_storage
  storage_type         = var.db_storage_type
  engine               = var.db_engine
  engine_version       = var.db_engine_version
  identifier           = var.db_identifier
  instance_class       = var.db_instance_class
  name                 = var.db_name
  username             = var.db_username
  password             = var.db_password
  storage_encrypted    = true
  vpc_security_group_ids  = [aws_security_group.rds.id]
  skip_final_snapshot  = true
  db_subnet_group_name = aws_db_subnet_group.db_subnet_group.name

  tags = local.common_tags
}
/*
resource "aws_db_snapshot" "db_snapshot" {
  db_instance_identifier = aws_db_instance.db_instance.id
  db_snapshot_identifier = var.db_snapshot_identifier
}
*/

resource "aws_db_subnet_group" "db_subnet_group" {
  name       = "${var.resource-prefix}-db-subnet-group"
  subnet_ids = aws_subnet.public[*].id

  tags = local.common_tags
}