resource "aws_ssm_parameter" "dt_postgres_db" {
  name  = "${var.resource-prefix}-postgres-database"
  type  = "String"
  value = aws_db_instance.db_instance.name
}

resource "aws_ssm_parameter" "dt_postgres_host" {
  name  = "${var.resource-prefix}-postgres-host"
  type  = "String"
  value = aws_db_instance.db_instance.address
}

resource "aws_ssm_parameter" "dt_postgres_port" {
  name  = "${var.resource-prefix}-postgres-port"
  type  = "String"
  value = aws_db_instance.db_instance.port
}

resource "aws_ssm_parameter" "dt_postgres_user" {
  name  = "${var.resource-prefix}-postgres-user"
  type  = "String"
  value = aws_db_instance.db_instance.username
}

resource "aws_ssm_parameter" "dt_postgres_password" {
  name  = "${var.resource-prefix}-postgres-password"
  type  = "String"
  value = aws_db_instance.db_instance.password
}