resource "aws_ecr_repository" "ecr_repository" {
  name                 = "${var.resource-prefix}/qledger"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}