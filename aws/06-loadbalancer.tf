# Application Load Balancer
resource "aws_lb" "main" {
  name               = "${var.resource-prefix}-lb"
  internal           = false
  load_balancer_type = "application"
  subnets            = flatten([aws_subnet.public.*.id])
  security_groups    = [aws_security_group.lb.id]

  enable_deletion_protection = false
}