# DevOps Takehome

We are looking for your skills to setup infrastructure from scratch

### Getting your work to us
Please send a single zip file named devops-takehome-{yourname}.zip containing:

 1. One folder containing the technical test.

## Technical Test

### Instructions
 - You need to provision an app connecting to a sql database.
 - The deployment needs to be highly available (HA), multiple zones or regions
 - Pick an application that depends on a database to deploy, here is a suggestion:
    - https://github.com/RealImage/QLedger

Requirements
 - In your solution please emphasize on readability, maintainability and DevOps methodologies. We expect a clear way to recreate your setup.
 - Use any configuration management tool.
 - The infrastructure provider can be either Google Cloud or AWS.
 - You can create the solution using any tools of your choice.
 - You should provide clear instructions on how to use the code you have provided. The clarity and precision of these instructions - and the ease with which the interviewers can execute them - will be a key part of the assessment. Please create a README file detailing said instructions. Please also use this file for listing any additional comments or observations you might want to share about your submission.

Bonus points:
 - Use some of the current tools we use: Terraform, Kubernetes
